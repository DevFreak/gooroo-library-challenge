﻿using System;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Gooroo_Library.Filters
{
    // Creates a custom AntiForgeryToken validation to allow Angular HTTP POST requests to be secure
    public class ValidateCustomAntiForgeryToken : FilterAttribute, IAuthorizationFilter
    {
        private const string KEY_NAME = "__RequestVerificationToken";

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            string clientToken = filterContext.RequestContext.HttpContext.Request.Headers.Get(KEY_NAME);
            if (clientToken == null) throw new HttpAntiForgeryException(String.Format("Header does not contain {0}", KEY_NAME));

            string serverToken = filterContext.HttpContext.Request.Cookies.Get(KEY_NAME).Value;
            if (serverToken == null) throw new HttpAntiForgeryException(String.Format("Cookies does not contain {0}", KEY_NAME));

            AntiForgery.Validate(serverToken, clientToken);
        }
    }
}