﻿using Gooroo_Library.DAL;
using Gooroo_Library.Filters;
using Gooroo_Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Gooroo_Library.Controllers
{
    public class BorrowersController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: Borrowers
        public ActionResult Index()
        {
            return View(unitOfWork.BorrowerRepository.GetBorrowers());
        }

        // GET: Borrowers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Borrowers/Create
        [HttpPost]
        [ValidateCustomAntiForgeryToken]
        public JsonResult Create(string firstName, string lastName)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Borrower newBorrower = new Borrower();

                    newBorrower.Id = unitOfWork.BorrowerRepository.GetNextId();
                    newBorrower.FirstName = firstName;
                    newBorrower.LastName = lastName;

                    unitOfWork.BorrowerRepository.InsertBorrower(newBorrower);

                    return Json(new { info = "Borrower has been successfully added.", url = "/Borrowers/Index" });
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json("Unable to save changes.Try again, and if the problem persists contact your system administrator. Error: " + e.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return Json("An error occurred, please try again.");
        }
    }
}
