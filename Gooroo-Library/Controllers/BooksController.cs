﻿using Gooroo_Library.DAL;
using Gooroo_Library.Models;
using Gooroo_Library.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Gooroo_Library.Controllers
{
    public class BooksController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: Books
        public ActionResult Index()
        {
            ViewBag.BorrowerId = unitOfWork.BorrowerRepository.GetBorrowers();

            return View(unitOfWork.BookRepository.GetBooks());
        }

        // GET: Books/Details/5
        public ActionResult Details(int id)
        {
            Book book = unitOfWork.BookRepository.GetBookByID(id);

            if (book.BorrowerId != 0)
            {
                ViewBag.BorrowerName = unitOfWork.BorrowerRepository.GetBorrowerByID(book.BorrowerId).FullName;
            }

            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        [HttpPost]
        [ValidateCustomAntiForgeryToken]
        public JsonResult Create(string name, string author)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Book newBook = new Book();
                    
                    newBook.Id = unitOfWork.BookRepository.GetNextId();
                    newBook.Name = name;
                    newBook.Author = author;
                    newBook.IsAvailable = true;
                    newBook.IsOverdue = false;

                    unitOfWork.BookRepository.InsertBook(newBook);

                    return Json(new { info = "Book has been successfully added.", url = "/Books/Index" });
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json("Unable to save changes.Try again, and if the problem persists contact your system administrator. Error: " + e.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return Json("An error occurred, please try again.");
        }

        // POST: Books/BorrowBook
        [HttpPost]
        [ValidateCustomAntiForgeryToken]
        public JsonResult BorrowBook(int bookId, DateTime borrowingDate, int borrowerId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Book book = new Book();

                    book.Id = bookId;
                    book.BorrowerId = borrowerId;
                    book.BorrowingDate = borrowingDate;

                    unitOfWork.BookRepository.BorrowBook(book);

                    return Json(new { info = "Book has been successfully borrowed.", url = "/Books/Index" });
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json("Unable to save changes.Try again, and if the problem persists contact your system administrator. Error: " + e.Message);
            }

            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return Json("An error occurred, please try again.");
        }
    }
}
