﻿using Gooroo_Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gooroo_Library.DAL
{
    public interface IBorrowerRepository
    {
        int GetNextId();
        IEnumerable<Borrower> GetBorrowers();
        Borrower GetBorrowerByID(int borrowerId);
        void InsertBorrower(Borrower borrower);
    }
}