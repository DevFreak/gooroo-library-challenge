﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gooroo_Library.Models;

namespace Gooroo_Library.DAL
{
    public class BorrowerRepository : IBorrowerRepository
    {
        private static List<Borrower> borrowers;

        public static List<Borrower> Borrowers
        {
            get
            {
                if (borrowers == null)
                    borrowers = InitBorrowers();

                return borrowers;
            }
        }

        private static List<Borrower> InitBorrowers()
        {
            return new List<Borrower>() {
                new Borrower()
                {
                    Id = 1,
                    FirstName = "Frank",
                    LastName = "Brown"
                },

                new Borrower()
                {
                    Id = 2,
                    FirstName = "Louise",
                    LastName = "Crossley"
                },

                new Borrower()
                {
                    Id = 3,
                    FirstName = "David",
                    LastName = "Bonavia"
                },

                new Borrower()
                {
                    Id = 4,
                    FirstName = "Marshall",
                    LastName = "Smith"
                },

                new Borrower()
                {
                    Id = 5,
                    FirstName = "Sam",
                    LastName = "Geller"
                }
            };
        }

        public int GetNextId()
        {
            return Borrowers.Count + 1;
        }

        public IEnumerable<Borrower> GetBorrowers()
        {
            return Borrowers;
        }

        public Borrower GetBorrowerByID(int borrowerId)
        {
            return Borrowers.First(u => u.Id == borrowerId);
        }

        public void InsertBorrower(Borrower borrower)
        {
            Borrowers.Add(borrower);
        }
    }
}