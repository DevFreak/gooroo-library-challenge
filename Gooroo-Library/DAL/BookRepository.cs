﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gooroo_Library.Models;

namespace Gooroo_Library.DAL
{
    public class BookRepository : IBookRepository
    {
        private static List<Book> books;

        public static List<Book> Books
        {
            get
            {
                if (books == null)
                    books = InitBooks();

                return books;
            }
        }

        private static List<Book> InitBooks()
        {
            return new List<Book>() {
                new Book()
                {
                    Id = 1,
                    Name = "Physics of Impossible",
                    Author = "Michio Kaku",
                    IsAvailable = false,
                    IsOverdue = false,
                    BorrowerId = 1,
                    BorrowingDate = new DateTime(2017, 8, 29)
                },

                new Book()
                {
                    Id = 2,
                    Name = "Astrophysics for People in a Hurry",
                    Author = "Neil deGrasse Tyson",
                    IsAvailable = true,
                    IsOverdue = false
                },

                new Book()
                {
                    Id = 3,
                    Name = "The Divine Comedy",
                    Author = "Dante Alighieri",
                    IsAvailable = false,
                    IsOverdue = true,
                    BorrowerId = 4,
                    BorrowingDate = new DateTime(2017, 8, 10)
                },

                new Book()
                {
                    Id = 4,
                    Name = "Sapiens: A Brief History of Humankind",
                    Author = "Yuval Noah Harari",
                    IsAvailable = true,
                    IsOverdue = false
                },

                new Book()
                {
                    Id = 5,
                    Name = "The Dragons of Eden",
                    Author = "Carl Sagan",
                    IsAvailable = false,
                    IsOverdue = true,
                    BorrowerId = 2,
                    BorrowingDate = new DateTime(2017, 8, 25)
                },
            };
        }

        public int GetNextId()
        {
            return Books.Count() + 1;
        }

        public IEnumerable<Book> GetBooks()
        {
            return Books;
        }

        public Book GetBookByID(int bookId)
        {
            return Books.First(u => u.Id == bookId);
        }

        public void InsertBook(Book Book)
        {
            Books.Add(Book);
        }

        public void BorrowBook(Book book)
        {
            Book bookToBorrow = Books.First(u => u.Id == book.Id);

            bookToBorrow.IsAvailable = false;
            bookToBorrow.BorrowerId = book.BorrowerId;
            bookToBorrow.BorrowingDate = book.BorrowingDate;

            if ((DateTime.Now.Date - book.BorrowingDate.Date).TotalDays / 7 > 2)
                bookToBorrow.IsOverdue = true;
        }
    }
}