﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gooroo_Library.DAL
{
    public class UnitOfWork
    {
        private BookRepository bookRepository;
        private BorrowerRepository borrowerRepository;

        public BookRepository BookRepository
        {
            get
            {
                if (this.bookRepository == null)
                {
                    this.bookRepository = new BookRepository();
                }

                return bookRepository;
            }
        }

        public BorrowerRepository BorrowerRepository
        {
            get
            {
                if (this.borrowerRepository == null)
                {
                    this.borrowerRepository = new BorrowerRepository();
                }

                return borrowerRepository;
            }
        }
    }
}