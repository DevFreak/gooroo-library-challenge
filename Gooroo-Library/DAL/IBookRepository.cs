﻿using Gooroo_Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gooroo_Library.DAL
{
    public interface IBookRepository
    {
        int GetNextId();
        IEnumerable<Book> GetBooks();
        Book GetBookByID(int bookId);
        void InsertBook(Book Book);
    }
}