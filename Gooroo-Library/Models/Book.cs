﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gooroo_Library.Models
{
    public class Book
    {
        public int Id { get; set; }

        [Display(Name = "Borrower")]
        public int BorrowerId { get; set; }

        [Display(Name = "Book Name")]
        public string Name { get; set; }

        [Display(Name = "Book Author")]
        public string Author { get; set; }

        [Display(Name = "Date of Borrow")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime BorrowingDate { get; set; }

        [Display(Name = "Is Available")]
        public bool IsAvailable { get; set; }

        [Display(Name = "Is Overdue")]
        public bool IsOverdue { get; set; }
    }
}